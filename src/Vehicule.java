/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author stag
 */
public class Vehicule {
    
    private String couleur;
    private String marque;
    private String modele;
    private int compteur = 0;
    private boolean demarre = false;
    private String type;

    //Constructor
    public Vehicule() {
        this.couleur = "blanc";
        this.marque = "renault";
        this.modele = "megane";
    }

    //setter
    public void setMarque(String marque) {
        if (marque == "Renault" || marque == "Dacia") {
            this.marque = marque;
        } else {
            System.err.println("La marque doit être Renault ou Dacia !");

        }
    }

    public void setCouleur(String couleur) {
        if (couleur == "bleu" || couleur == "blanc" || couleur == "rouge") {
            this.couleur = couleur;
        } else {
            System.err.println("La couleur doit être bleu, blanc ou rouge !! :" + couleur);
        }
    }

    public void setCompteur(int compteur) {
        if (compteur >= 0) {
            this.compteur = compteur;
        } else {
            System.err.println("Pas de valeur négative pour le compteur !!!");
        }
    }

    public void setModele(String modele) {
        this.modele = modele;
    }

    public void setDemarre(boolean demarre) {
        this.demarre = demarre;
    }

    public void setType(String type) {
        this.type = type;
    }
    

    // Getter
    public String getCouleur() {
        return couleur;
    }

    public String getMarque() {
        return marque;
    }

    public String getModele() {
        return modele;
    }

    public int getCompteur() {
        return compteur;
    }

    public boolean isDemarre() {
        return demarre;
    }

    public String getType() {
        return type;
    }
    
    

    //methodes
    public void demarrer() {
        demarre = true;
    }

    public void avancer(int km) {
        if (km >= 0) {
            compteur += km;

        } else {
            System.err.println("Pas de valeur négative pour les km !!!");
        }
    }

    public void arreter() {
        demarre = false;
    }

    public void afficher() {
        System.out.println("-------Infos -----------------------");
        System.out.println("type :" + type);
        System.out.println("Couleur =" + couleur);
        System.out.println("Marque =" + marque);
        System.out.println("Modele =" + modele);
        System.out.println("compteur =" + compteur);
        System.out.println("Moteur demarré ? =" + demarre);

    }
    
}
